//
//  Ecran25ViewController.swift
//  NuBloca
//
//  Created by gabriel on 5/27/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit



class Ecran25ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var menuNameArr:Array = [String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        menuNameArr = ["Romania","Bulgaria","Italia","Germania","Spania","Franta"]
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellNew") as! TableViewCellNew
        let cell = Bundle.main.loadNibNamed("AlegeTaraCellView", owner: self, options: nil)?.first as! AlegeTaraCellView
        
        //cell.imgIcon.image = iconeImage[indexPath.row]
        cell.labelTara.text = menuNameArr[indexPath.row]
        
        return cell
    }
    
    //    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //
    //        let cell:TableViewCell23 = tableView.cellForRow(at: indexPath) as! TableViewCell23
    //
    //        //cell.textCell23.setImage(UIImage(named: "radio_press.png"), for: .normal)
    //        cell.radioCell23.image = UIImage(named: "radio_press.png")
    //    }
}

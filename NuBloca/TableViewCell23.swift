//
//  TableViewCell23.swift
//  NuBloca
//
//  Created by gabriel on 5/26/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit

class TableViewCell23: UITableViewCell {
    
    @IBOutlet var radioCell23: UIImageView!
    
    @IBOutlet var textCell23: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

//
//  MenuViewController.swift
//  NuBloca
//
//  Created by gabriel on 5/23/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var menuNameArr:Array = [String]()
    var iconeImage:Array = [UIImage]()
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        menuNameArr = ["Ajutor","Contact","Despre","Termeni si Conditii","Share","Contul meu"]
        iconeImage = [UIImage(named:"home")!,UIImage(named:"message")!,UIImage(named:"map")!,UIImage(named:"setting")!,UIImage(named:"home")!,UIImage(named:"message")!]
        
        //imgProfile.layer.borderWidth = 2
        //imgProfile.layer.borderColor = UIColor.green.cgColor
        //imgProfile.layer.cornerRadius = 50
        
        imgProfile.layer.masksToBounds = false
        imgProfile.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
        cell.imgIcon.image = iconeImage[indexPath.row]
        cell.iblMenuName.text! = menuNameArr[indexPath.row]
        return cell
    }
    
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revealViewController:SWRevealViewController = self.revealViewController()
        
        let cell:MenuTableViewCell = tableView.cellForRow(at: indexPath) as! MenuTableViewCell
        
        print(cell.iblMenuName.text!)
        
        if cell.iblMenuName.text! == "Despre"
        {
            
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "despre") as! Ecran9ViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealViewController.pushFrontViewController(newFrontController, animated: true)
            
        }
        if cell.iblMenuName.text! == "Contul meu"
        {
            
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "Ecran14ViewController") as! Ecran14ViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealViewController.pushFrontViewController(newFrontController, animated: true)
            
        }
        if cell.iblMenuName.text! == "Termeni si Conditii"
        {
            
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "Tos") as! Ecran3ViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealViewController.pushFrontViewController(newFrontController, animated: true)
            
        }
        if cell.iblMenuName.text! == "Ajutor"
        {
            
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "Ajutor") as! Ecran11ViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealViewController.pushFrontViewController(newFrontController, animated: true)
            
        }
        if cell.iblMenuName.text! == "Contact"
        {
            
            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "Contact") as! Ecran10ViewController
            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
            
            revealViewController.pushFrontViewController(newFrontController, animated: true)
            
        }
//        if cell.iblMenuName.text! == "Message"
//        {
//            print("message Tapped")
//            
//            let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
//            let newFrontController = UINavigationController.init(rootViewController: newViewcontroller)
//            
//            revealViewController.pushFrontViewController(newFrontController, animated: true)
//        }
//        if cell.iblMenuName.text! == "Map"
//        {
//            print("Map Tapped")
//        }
//        if cell.iblMenuName.text! == "Setting"
//        {
//            print("setting Tapped")
//        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

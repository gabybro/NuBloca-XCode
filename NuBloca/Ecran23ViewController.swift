//
//  Ecran23ViewController.swift
//  NuBloca
//
//  Created by gabriel on 5/26/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit



class Ecran23ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    var menuNameArr:Array = [String]()
    let imageName = "radio_press.png"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        menuNameArr = ["Ajutor","Contact","Despre","Termeni si Conditii","Share","Contul meu"]
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCellNew") as! TableViewCellNew
        let cell = Bundle.main.loadNibNamed("TableViewCell23", owner: self, options: nil)?.first as! TableViewCell23
        
        //cell.imgIcon.image = iconeImage[indexPath.row]
        cell.textCell23.text = menuNameArr[indexPath.row]
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        let cell:TableViewCell23 = tableView.cellForRow(at: indexPath) as! TableViewCell23
//        
//        //cell.textCell23.setImage(UIImage(named: "radio_press.png"), for: .normal)
//        cell.radioCell23.image = UIImage(named: "radio_press.png")
//    }
}

//
//  Ecran1ViewController.swift
//  NuBloca
//
//  Created by gabriel on 5/17/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit

class Ecran1ViewController: UIViewController, BWWalkthroughViewControllerDelegate {
    
    
    
    
    @IBOutlet weak var progressView: UIProgressView!
    
    
   
     func goToMainScreen() {
        
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "mainScreen")
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
     func goToAuthScreen() {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ecran31Auth")
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.isNavigationBarHidden = true
        
        
        requestData()
        
        
        Timer.scheduledTimer(timeInterval: 0.35, target: self, selector: #selector(Ecran1ViewController.updateProgressView), userInfo: nil, repeats: true)
        
        
        progressView.setProgress(0, animated: false)
        
        
    }
    
    func updateProgressView(){
        if progressView.progress != 1{
            self.progressView.progress += 2 / 10
        } else {
            goToScreen()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        

    }
    
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
    func goToScreen(){
    
        let userDefaults = UserDefaults.standard
        
        if !userDefaults.bool(forKey: "walkthroughPresented") {
            

            showWalkthrough()
            
            userDefaults.set(true, forKey: "walkthroughPresented")
            userDefaults.synchronize()
        } else {
            
            if !userDefaults.bool(forKey: "authStatus") {
                goToAuthScreen()
            }else {
                goToMainScreen()
            }
        }
    }
    
    @IBAction func showWalkthrough(){
        
        
        // Get view controllers and build the walkthrough
        let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
        let walkthrough = stb.instantiateViewController(withIdentifier: "walk") as! BWWalkthroughViewController
        let page_zero = stb.instantiateViewController(withIdentifier: "walk0")
        let page_one = stb.instantiateViewController(withIdentifier: "walk1")
        let page_two = stb.instantiateViewController(withIdentifier: "walk2")
        let page_three = stb.instantiateViewController(withIdentifier: "walk3")
        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController:page_one)
        walkthrough.add(viewController:page_two)
        walkthrough.add(viewController:page_three)
        walkthrough.add(viewController:page_zero)
        
        self.dismiss(animated: false, completion: nil)

        self.present(walkthrough, animated: true, completion: nil)
        
    }

    // MARK: - Walkthrough delegate -
    
    func walkthroughPageDidChange(_ pageNumber: Int) {
        print("Current Page \(pageNumber)")
    }
    
    func walkthroughCloseButtonPressed() {
        self.dismiss(animated: true, completion: nil)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)        
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "createAcc")
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
   
    
    func requestData(){
    //let urll = URL(string: "http://api.fixer.io/latest")
    let urll = URL(string: "http://api.nubloca.ro/ecran1/request1")
    var request = URLRequest(url:urll!)
    request.httpMethod = "BODYGET"
    let tara: [String: String] = ["tara":"ro"]
    let parameters: [String: AnyObject] = [
    "trimise": tara as AnyObject
    ]
    //let parameters = NSJSONSerialize.isValidJSONObject(jsonObject)
    do {
    request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
    
    } catch let error {
    print(error.localizedDescription)
    }
    
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = URLSession.shared.dataTask(with: request as URLRequest) {
    data, response, error in
    if error != nil
    {
    print("error")
    }
    else
    {
    if let content = data
    {
    do
    {
    let myJson = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
    
    if let rates = myJson["rates"] as? NSDictionary
    {
    if let currency = rates["GBP"]
    {
    print(currency)
    }
    }
    }
    catch
    {
    
    }
    }
    }
    }
    task.resume()
    }
    
    
    
}


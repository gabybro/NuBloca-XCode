//
//  Ecran31ViewController.swift
//  NuBloca
//
//  Created by gabriel on 5/19/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit


class Ecran31ViewController: UIViewController {
    
    
    @IBAction func btnParolaUitata(_ sender: Any) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "parolaUitata")
        self.present(nextViewController, animated:true, completion:nil)
        
    }
    
    
    @IBAction func btnGoToMain(_ sender: Any) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: "authStatus")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "mainScreen")
        self.present(nextViewController, animated:true, completion:nil)

    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

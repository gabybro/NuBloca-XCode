//
//  Ecran30ViewController.swift
//  NuBloca
//
//  Created by gabriel on 5/21/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit


class Ecran30ViewController: UIViewController {
    
    @IBAction func btnInregistrare(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ecran31Auth")
        self.dismiss(animated: false, completion: nil)
        self.present(nextViewController, animated:true, completion:nil)

    }
    
    @IBAction func btnTos(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "Tos")
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

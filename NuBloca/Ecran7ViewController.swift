//
//  Ecran7ViewController.swift
//  NuBloca
//
//  Created by gabriel on 5/19/17.
//  Copyright © 2017 gabriel. All rights reserved.
//

import UIKit

class Ecran7ViewController: UIViewController {
    
    
//    @IBAction func buttonMasinileTale(_ sender: Any) {
//        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "masinileTale")
//        self.present(nextViewController, animated:true, completion:nil)
//        
//    }

    @IBOutlet weak var Open: UIBarButtonItem!
    
    
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image : UIImage = UIImage (named: "logo.png")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView

        // Do any additional setup after loading the view.
        
        Open.target = self.revealViewController()
        Open.action = Selector("revealToggle:")
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
